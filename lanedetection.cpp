#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <math.h>

#define PIX(img,i,j,k) (((uchar*)img->imageData)[(i)*img->widthStep+(j)*img->nChannels+(k)])
#define PI 3.142857
#define SCALE_FACTOR_DIST 1
#define SCALE_FACTOR_ANG 1
// #define PLOT_HOUGH_LINES_THRESH 100
int PLOT_HOUGH_LINES_THRESH = 100;
// #define LINE_TOLERANCE_DISTANCE 100
int LINE_TOLERANCE_DISTANCE = 100;
// #define LINE_TOLERANCE_ANGLE 30
int LINE_TOLERANCE_ANGLE = 30;

using namespace std;
CvCapture *capture;

bool compare(const pair< pair<int, int>, int> &i, const pair< pair<int, int>, int> &j){
	// if(PIX(matrix, i.first, i.second, 0) >= PIX(matrix, j.first, j.second, 0))
	if(i.second >= j.second)
		return true;
	else 
		return false;
}

void uselower(IplImage *frame){
	// to blacken upper half of the image
	for(int i=0; i<frame->height; i++){
		for(int j=0; j<frame->width; j++){
			if(i<frame->height/2)
				PIX(frame, i, j, 0) = 0;
		}
	}
}

void superimpose(IplImage *lines_all, const IplImage *edges_canny){
	// superimposes white pixels of lines_all with that of edges_canny, retain intersection/common white pixels
	for(int i=0; i<lines_all->height; i++){
		for(int j=0; j<lines_all->width; j++){
			PIX(lines_all, i, j, 0) = (PIX(edges_canny, i, j, 0) == 0)? 0: PIX(lines_all, i, j, 0);
		}
	}
}

class lanedetection{
public:

	void alternateplotpath(IplImage *path, const IplImage *matrix, int linecount){
		// use linecount = 0 to print all lines
		vector< pair< pair<int, int>, int> > selected;

		// store all dist-theta pairs with value greater than threshold
		// removes multiple entry of dist-theta pairs with values below tolerance
		for(int i=0; i<matrix->height; i++){
			for(int j=0; j<matrix->width; j++){
				if(PIX(matrix, i, j, 0) > PLOT_HOUGH_LINES_THRESH){
					int max  =  PIX(matrix, i, j, 0);
					bool flag = true;
					for(int k = i-4; k<= i+4; k++){
						for(int m = j-4; m<= j+4; m++){
							if(max <= PIX(matrix, k, m, 0) && i!=k && j!= m)
								flag = false;
						}
					}
					if(flag)
						selected.push_back( make_pair( make_pair(i, j), PIX(matrix, i,j, 0) ) );
				}
			}
		}

		for(int i=0; i<selected.size(); i++){
			for(int j=i+1; j<selected.size();){
				int a = fabs(selected[i].first.first - selected[j].first.first);
				int b = fabs(selected[i].first.second - selected[j].first.second);
				if(a<LINE_TOLERANCE_DISTANCE && b<LINE_TOLERANCE_ANGLE){
					// cout<<(selected[j]).first<<":"<<(selected[j]).first<<" - ";
					// selected[i].first.first += a/2;
					// selected[i].first.second += b/2;
					selected.erase(selected.begin() + j);
					// cout<<(selected[j]).first<<":"<<(selected[j]).first<<" - "<<endl;;
					// getchar();
				}
				else j++;
			}
		}

		if(linecount && linecount <= selected.size()){
			sort(selected.begin(), selected.end(), compare);
			selected.erase(selected.begin()+linecount, selected.end());
		}
		// cout<<selected.size()<<"\n";
		// for(int i=0; i<selected.size(); i++)
			// cout<<(selected[i]).first.first<<":"<<(selected[i]).first.second<<" - "<<selected[i].second<<endl;
		// getchar();

		// mark white paths in black background
		for(int i=0; i<path->height; i++){
			for(int j=0; j<path->width; j++){
				bool flag = false;
				for(int k=0; k<selected.size(); k++){
					// iterativey check in array of pixels in matrix above thresh
					double theta = (double)(selected[k].first.second)*SCALE_FACTOR_ANG*PI/180;
					int index = int( j*cos(theta) + i*sin(theta) );
					index /= SCALE_FACTOR_DIST;
					if(index == selected[k].first.first)
						flag = true;
				}
				PIX(path, i, j, 0) = (flag)? 255: 0;
			}
		}

		// clean up
		selected.clear();	
	}

	void plotpath(IplImage *path, const IplImage *matrix, int linecount){
		// use linecount = 0 to print all lines
		vector< pair< pair<int, int>, int> > selected;

		// store all dist-theta pairs with value greater than threshold
		for(int i=0; i<matrix->height; i++){
			for(int j=0; j<matrix->width; j++){
				if(PIX(matrix, i, j, 0) > PLOT_HOUGH_LINES_THRESH)
					selected.push_back( make_pair( make_pair(i, j), PIX(matrix, i,j, 0) ) );
			}
		}

		// removes multiple entry of dist-theta pairs with values below tolerance
		for(int i=0; i<selected.size(); i++){
			for(int j=i+1; j<selected.size();){
				int a = fabs(selected[i].first.first - selected[j].first.first);
				int b = fabs(selected[i].first.second - selected[j].first.second);
				if(a<LINE_TOLERANCE_DISTANCE && b<LINE_TOLERANCE_ANGLE){
					// cout<<(selected[j]).first<<":"<<(selected[j]).first<<" - ";
					// selected[i].first.first += a/2;
					// selected[i].first.second += b/2;
					selected.erase(selected.begin() + j);
					// cout<<(selected[j]).first<<":"<<(selected[j]).first<<" - "<<endl;;
					// getchar();
				}
				else j++;
			}
		}

		if(linecount && linecount <= selected.size()){
			sort(selected.begin(), selected.end(), compare);
			selected.erase(selected.begin()+linecount, selected.end());
		}
		// cout<<selected.size()<<"\n";
		// for(int i=0; i<selected.size(); i++)
			// cout<<(selected[i]).first.first<<":"<<(selected[i]).first.second<<" - "<<selected[i].second<<endl;
		// getchar();

		// mark white paths in black background
		for(int i=0; i<path->height; i++){
			for(int j=0; j<path->width; j++){
				bool flag = false;
				for(int k=0; k<selected.size(); k++){
					// iterativey check in array of pixels in matrix above thresh
					double theta = (double)(selected[k].first.second)*SCALE_FACTOR_ANG*PI/180;
					int index = int( j*cos(theta) + i*sin(theta) );
					index /= SCALE_FACTOR_DIST;
					if(index == selected[k].first.first)
						flag = true;
				}
				PIX(path, i, j, 0) = (flag)? 255: 0;
			}
		}

		// clean up
		selected.clear();	
	}

	void plotlines(IplImage *lines_all, const IplImage *matrix){

		vector< pair<int, int> > selected;
		// store all dist-theta pairs with value greater than threshold
		for(int i=0; i<matrix->height; i++){
			for(int j=0; j<matrix->width; j++){
				if(PIX(matrix, i, j, 0) > PLOT_HOUGH_LINES_THRESH)
					selected.push_back( make_pair(i, j) );
			}
		}

		// removes multiple entry of dis-theta pairs with values below tolerance
		for(int i=0; i<selected.size(); i++){
			for(int j=i+1; j<selected.size();){
				int a = fabs(selected[i].first - selected[j].first);
				int b = fabs(selected[i].second - selected[j].second);
				if(a<LINE_TOLERANCE_DISTANCE && b<LINE_TOLERANCE_ANGLE){
					// cout<<(selected[j]).first<<":"<<(selected[j]).first<<" - ";
					// selected[i].first += a/2;
					// selected[i].second += b/2;
					selected.erase(selected.begin() + j);
					// cout<<(selected[j]).first<<":"<<(selected[j]).first<<" - "<<endl;;
					// getchar();
				}
				else j++;
			}
		}

		// mark white paths in black background
		for(int i=0; i<lines_all->height; i++){
			for(int j=0; j<lines_all->width; j++){
				bool flag = false;
				for(int k=0; k<selected.size(); k++){
					// iterativey check in array of pixels in matrix above thresh
					double theta = (double)(selected[k].second)*SCALE_FACTOR_ANG*PI/180;
					int index = int( j*cos(theta) + i*sin(theta) );
					index /= SCALE_FACTOR_DIST;
					if(index == selected[k].first)
						flag = true;
				}
				PIX(lines_all, i, j, 0) = (flag)? 255: 0;
			}
		}

		// clean up
		selected.clear();
	}

	void houghlines(IplImage *matrix, const IplImage *edges_canny){

		// initialise matrix to black image
		for(int i=0; i<matrix->height; i++){
			for(int j=0; j<matrix->width; j++){
				PIX(matrix, i, j, 0) = 0;
			}
		}

		// hough transform values update
		for(int i=0; i<edges_canny->height; i++){
			for(int j=0; j<edges_canny->width; j++){
				if(PIX(edges_canny, i, j, 0) == 255){
					for(int k=0; k<matrix->width; k++){
						double theta = (double)k*SCALE_FACTOR_ANG*PI/180;
						int distance = int( j*cos(theta) + i*sin(theta) );
						distance /= SCALE_FACTOR_DIST;
						if(distance >= 0 && distance < matrix->height)
							PIX(matrix, distance, k , 0)++;
					}
				}
			}
		}

		// Scale to max val of plot - Optional
		// BEGIN
		int max = PIX(matrix, 0, 0, 0);
		for(int i=0; i<matrix->height; i++){
			for(int j=0; j<matrix->width; j++){
				max = (PIX(matrix, i, j, 0) > max)? PIX(matrix, i, j, 0) : max;
			}
		}

		for(int i=0; i<matrix->height; i++){
			for(int j=0; j<matrix->width; j++){
				PIX(matrix, i, j, 0) = (double)PIX(matrix, i, j, 0)*255/max;
			}
		}	
		// END
	}

}lanedetection;

void onTrackbarSlide(int seek){
	cvSetCaptureProperty(capture, CV_CAP_PROP_POS_FRAMES, seek);
}

int main()
{
	// create and verify capture
	capture = cvCreateFileCapture("./newVideo.avi");
	if(!capture){
		printf("No capture loaded!\n");
		getchar();
		exit(0);
	}

	IplImage *frame, *grayimg, *edges_canny, *matrix, *lines_all, *path;
	int dist, ang;

	frame = cvQueryFrame(capture);
	if(!frame){
		printf("No frames loaded!\n");
		getchar();
		exit(0);
	}	

	// create/capture images
	grayimg = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 1);
	edges_canny = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 1);
	lines_all = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 1);
	path = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 1);


	// create image for hough transform
	dist = sqrt(pow(frame->height, 2) + pow(frame->width, 2));
	dist /= SCALE_FACTOR_DIST;
	ang =  360 / SCALE_FACTOR_ANG;
	matrix = cvCreateImage(cvSize(ang, dist), IPL_DEPTH_8U, 1);

	// create windows
	cvNamedWindow("input",CV_WINDOW_AUTOSIZE);
	cvNamedWindow("edges_canny", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("matrix", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("lines_all", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("path", CV_WINDOW_AUTOSIZE);

	// create trackbars
	int low = 50, high = 250;
	cvCreateTrackbar("low", "edges_canny", &low, 255);
	cvCreateTrackbar("high", "edges_canny", &high, 255);
	cvCreateTrackbar("LINE_TOLERANCE_DISTANCE", "lines_all", &LINE_TOLERANCE_DISTANCE, dist);
	cvCreateTrackbar("LINE_TOLERANCE_ANGLE", "lines_all", &LINE_TOLERANCE_ANGLE, 360);
	cvCreateTrackbar("PLOT_HOUGH_LINES_THRESH", "lines_all", &PLOT_HOUGH_LINES_THRESH, 360);

	// create seekbar
	int seek = 0;
	int x = cvGetCaptureProperty(capture,CV_CAP_PROP_FRAME_COUNT);
	cvCreateTrackbar("Seek", "edges_canny", &seek, x, onTrackbarSlide);

	while(1)
	{
		frame = cvQueryFrame(capture);

		if(!frame){
			printf("No more frames!\n");
			break;
		}
		// generate edges stream
		cvCvtColor(frame, grayimg, CV_BGR2GRAY);
		cvCanny(grayimg, edges_canny, low, high, 3);
		cvDilate(edges_canny, edges_canny);
		cvErode(edges_canny, edges_canny);

		// return hough-transformed lines-only image
		lanedetection.houghlines(matrix, edges_canny);
		lanedetection.plotpath(lines_all, matrix, 0);
		lanedetection.alternateplotpath(path, matrix, 0);
		// superimpose(lines_all, edges_canny);
		// uselower(lines_all);

		// display output
		cvShowImage("input",frame);
		cvShowImage("edges_canny",edges_canny);
		cvShowImage("matrix",matrix);
		cvShowImage("lines_all",lines_all);
		cvShowImage("path",path);
		cvSetTrackbarPos("Seek", "edges_canny", seek++);
		cvWaitKey(100);
	}

	cout<<"Processing complete. Program will now exit...\n";
	cvWaitKey(0);

	// clean up
	cvDestroyWindow("image");
	cvDestroyWindow("edges_canny");
	cvDestroyWindow("hough");
	cvDestroyWindow("lines_all");
	cvReleaseImage(&frame);
	cvReleaseImage(&edges_canny);
	cvReleaseImage(&matrix);
	cvReleaseImage(&lines_all);
}